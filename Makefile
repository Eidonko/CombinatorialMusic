all:	data2lily data2lily5 perm

data2lily:	data2lily3new.c
		gcc -O3 -o data2lily data2lily3new.c -lm

data2lily5:	data2lily5.c
		gcc -O3 -o data2lily5 data2lily5.c -lm

perm:	perm.w
	ctangle perm.w
	gcc -O3 -g -DPRINT -DTRI -o perm perm.c -lm

run:
	permusic.sh

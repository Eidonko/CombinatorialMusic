#!/usr/bin/env python3
#
# v1.0  First very preliminary version
# v1.1  In `frequencies`, `if (i + 1) % notes == 0:` was `if (i + 1) % PythaNotes == 0:`
#
# "Musica est exercitium arithmeticae occultum nescientis se numerare animi" (Leibniz)

import numpy as np

PythaNotes = 17
PythaSteps = [ 256/243.0, 2187/2048.0, 9/8.0, 32/27.0, 19683/16384.0, 81/64.0, 4/3.0, 1024/729.0, 729/512.0,
               3/2.0, 128/81.0, 6561/4096.0, 27/16.0, 16/9.0, 59049/32768.0, 243/128.0, 2/1.0 ]
xxxiNotes = 31
# Harmonic ratios
xxxiSteps = [ 1+1/31.0, 1+2/31.0, 1+3/31.0, 1+4/31.0, 1+5/31.0, 1+6/31.0, 1+7/31.0, 1+8/31.0, 1+9/31.0, 1+10/31.0,
        1+11/31.0, 1+12/31.0, 1+13/31.0, 1+14/31.0, 1+15/31.0, 1+16/31.0, 1+17/31.0, 1+18/31.0, 1+19/31.0, 1+20/31.0,
        1+21/31.0, 1+22/31.0, 1+23/31.0, 1+24/31.0, 1+25/31.0, 1+26/31.0, 1+27/31.0, 1+28/31.0, 1+29/31.0, 1+30/31.0,
        1+31/31.0 ]

def frequencies(n, notes, steps, freq):
    freqs = [ [ freq ] ]
    f = freq
    for i in range(0,n):
        j = i % notes
        freqs[-1].append(f * steps[j])
        if (i + 1) % notes == 0:
            f *= steps[-1]
            freqs.append([])
    return freqs

C1 = 8.1757989156
C2 = C1*2
C3 = C1*4
C4 = C1*8
C5 = C1*16

def linearmap(a, b, c, d, x):
    try:
        res = int(round(c + ((d-c)/(b-a))*(x-a)))
    except:
        print("a={}, b={}, c={}, d={}, x={}".format(a,b,c,d,x))
        return 0
    return res

def PythagoreanFrequencies(n, notes=PythaNotes, steps=PythaSteps, freq=C4):
    return frequencies(n, notes, steps, freq)

# octaves = 5
# pf = PythagoreanFrequencies(PythaNotes * octaves)
#  print("pf[octave, note] = frequency")
#  for i in range(0,PythaNotes * octaves):
#      print("pf[{},      {}]    = {}".format(i // PythaNotes, i % PythaNotes, pf[i]))

def putFrequencies(data, datalen, octaves, n, scaleNotes, scaleSteps, initFreq, verbose=False):
    maxnow = np.amax(data, axis=None)
    minnow = np.amin(data, axis=None)
    maxend = octaves * scaleNotes
    factor = maxend / maxnow
    pf = frequencies(n, scaleNotes, scaleSteps, initFreq)
    if verbose:
        with open("pf.txt", 'w') as f:
            for epf in pf:
                f.write('{}\n'.format(epf))
        with open("steps.txt", 'w') as f:
            for epf in pf:
                f.write('{}, '.format(epf))
            f.write('\n\n'.format(epf))
    rdata = np.copy(data)
    r, c = data.shape
    for cc in range(0,c):
        for rr in range (0,r):
            rdata[rr, cc] = linearmap(minnow, maxnow, 0, maxend-1, data[rr, cc])
    if verbose:
        with open("rdata.txt", 'w') as f:
            f.write('linear mapping [{}, {}] onto [0, {}]:\n'.format(minnow, maxnow, maxend-1))
            for j in range(0, datalen):
                rdata[:,j].tofile(f, sep=",", format="%g")
    for cc in range(0,c):
        for rr in range (0,r):
            octa= int(rdata[rr, cc] // scaleNotes)
            nota= int(rdata[rr, cc] % scaleNotes)
            data[rr, cc] = pf[octa][nota]

    if verbose:
        with open("frequencies.txt", 'w') as f:
            for j in range(0, datalen):
                data[:,j].tofile(f, sep=",", format="%s")
                f.write('\n')
    return data

# run-length encoding of fdata
def rle(fdata):
    (r,c) = fdata.shape
    freql = []
    durl  = []
    for j in range(0, c):
        outfreq = []
        outdur = []
        dur = 0.2
        fdatum = fdata[0,j]
        for i in range (1, r):
            if fdata[i,j] == fdatum:
                dur += 0.2
            else:
                outfreq.append(fdatum)
                outdur.append(dur)
                dur = 0.2
                fdatum = fdata[i,j]
        outfreq.append(fdatum)
        outdur.append(dur)
        freql.append(outfreq)
        durl.append(outdur)
    return freql, durl

# frequencies and durations are used to create a SuperCollider source
def putPbinds(filename, freql, durl):
    with open(filename, 'w') as f:
        l = len(freql)
        f.write('(\n')
        f.write('s.record;\n')
        for j in range(0, l):
            f.write('    Pbind(\\freq,   Pseq({}),\n'.format(freql[j])) 
            f.write('          \\dur,    Pseq({}),\n'.format(durl[j])) 
            f.write('          \\amp,    Pseq([0.6], inf),\n') 
            f.write('          \\legato, 0.5\n') 
            f.write('    ).play;\n')
        f.write('s.stopRecording;\n')
        f.write(')\n')

# main functions of the module
def Pythagorize(data, datalen, octaves, baseFreq, outputfile):
    fq=putFrequencies(data, datalen, octaves, PythaNotes * octaves, PythaNotes, PythaSteps, baseFreq, True)
    freqs, durs = rle(fq)
    print(freqs)
    putPbinds(outputfile, freqs, durs)


def xxxize(data, datalen, octaves, baseFreq, outputfile):
    fq=putFrequencies(data, datalen, octaves, xxxiNotes * octaves, xxxiNotes, xxxiSteps, baseFreq, True)
    freqs, durs = rle(fq)
    putPbinds(outputfile, freqs, durs)
